package com.example.util;

public interface Constants {
	interface Status{
		public enum DeliveryBoyStatus {
			ACTIVE("Active"),INTRANSIT("InTransit");

			String status;
			private DeliveryBoyStatus(String status){
				this.status=status;
			}
		}
		
		public enum OrderStatus {
			PLACED("Placed"),PICKED("Picked"),DELIVERED("Delivered");

			String status;
			private OrderStatus(String status){
				this.status=status;
			}
		}		
	}
	interface GlobalConst{
		String NUMBER_OF_DELIVERY_BOYS = "totalDeliveryBoys";
	}

}
