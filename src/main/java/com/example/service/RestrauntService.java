package com.example.service;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.model.Order;
import com.example.repository.OrderDao;
import com.example.util.Constants;

@Service
public class RestrauntService {

	private final OrderDao orderDao;

	@Autowired
	public RestrauntService(OrderDao orderDao) {

		this.orderDao = orderDao;
	}

	public int createNewOrder(Order order) {

		order.setPlacedAt(new Date());
		order.setStatus(Constants.Status.OrderStatus.PLACED.name());
		orderDao.createOrder(order);
		return order.getId();
	}

	public Order updateOrder(Order order) throws Exception {
		return orderDao.updateOrderDetails(order);
	}

	public Order updateOrderStatus(int orderId, String status) {

		Order order = new Order();
		order.setId(orderId);
		order.setStatus(status);
		if (Constants.Status.OrderStatus.DELIVERED.name().equals(order.getStatus()))
			order.setDeliveredAt(new Date());
		else if (Constants.Status.OrderStatus.PICKED.name().equals(order.getStatus()))
			order.setPickedAt(new Date());

		return orderDao.updateOrderDetails(order);
	}

	public Order getOrderDetails(Integer orderId) {

		return orderDao.getOrderDetails(orderId);
	}

	public Order allotOrderToDeliveryBoy(int orderId, int deliveryBoyId) {
		// TODO Auto-generated method stub
		Order order = orderDao.getOrderDetails(orderId);
		if (order == null)
			throw new IllegalArgumentException("Order Not Found");
		if (!order.getStatus().equals(Constants.Status.OrderStatus.PLACED.name()))
			throw new IllegalArgumentException("Order cannot be picked");

		order.setDeliveryBoyId(deliveryBoyId);
		order.setStatus(Constants.Status.OrderStatus.PICKED.name());
		order.setPickedAt(new Date());
		return orderDao.updateOrderDetails(order);
	}

	public Order getOrderInTransit(int deliveryBoyId) {

		List<Order> orders = orderDao.getOrderInTransit(deliveryBoyId);
		if (orders == null || orders.size() == 0)
			throw new IllegalArgumentException("Order Not Associated with delivery boy");
		return orders.get(0);
	}

}
