package com.example.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.example.model.DeliveryBoy;
import com.example.repository.DeliveryBoyDao;
import com.example.util.Constants;

@Service
public class DeliveryService {

	private final DeliveryBoyDao deliveryBoyDao;

	@Autowired
	public DeliveryService(DeliveryBoyDao deliveryBoyDao) {

		this.deliveryBoyDao = deliveryBoyDao;
	}

	public List<DeliveryBoy> getActiveDeliveryBoys() {
		return deliveryBoyDao.getDeliveryByStatus(Constants.Status.DeliveryBoyStatus.ACTIVE.name());
	}

	public DeliveryBoy getDeliveryBoy(int deliveryBoyId) {

		DeliveryBoy deliveryBoy = deliveryBoyDao.getDeliveryBoy(deliveryBoyId);
		if (deliveryBoy == null)
			throw new IllegalArgumentException("No Data Found");

		return deliveryBoy;
	}

	public void updateDeliveryBoyStatus(int deliveryBoyId, String status) {

		DeliveryBoy deliveryBoy = getDeliveryBoy(deliveryBoyId);
		deliveryBoy.setStatus(status);
		deliveryBoyDao.updateDeliveryBoy(deliveryBoy);
	}
}
