package com.example.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.model.Order;
import com.example.service.RestrauntService;

@RestController
@RequestMapping("/")
public class RestrauntController {

	private final RestrauntService restrauntService;

	@Autowired
	public RestrauntController(RestrauntService restrauntService) {
		//
		this.restrauntService = restrauntService;
	}

	@PostMapping("order")
	public Order createOrder(@RequestBody Order order) throws Exception {
		if (order.getCustomerName() == null)
			throw new IllegalArgumentException("Customer Name is Required");
		if (order.getProductName() == null)
			throw new IllegalArgumentException("Product Name is Required");
		if (order.getQuantity() == 0)
			throw new IllegalArgumentException("Quantity cannot be empty");
		restrauntService.createNewOrder(order);
		return order;
	}

	@GetMapping("order")
	public Order getOrderDetails(@RequestParam int orderId) throws Exception {
		if (orderId == 0)
			throw new IllegalArgumentException("Order Id not recieved ");
		return restrauntService.getOrderDetails(orderId);
	}
}
