package com.example.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;

import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.model.DeliveryBoy;
import com.example.model.Order;
import com.example.service.DeliveryService;
import com.example.service.RestrauntService;
import com.example.util.Constants;

@RestController
public class DeliveryController {

	private final RestrauntService restrauntService;
	private final DeliveryService deliveryService;

	@Autowired
	public DeliveryController(RestrauntService restrauntService, DeliveryService deliveryService) {
		this.restrauntService = restrauntService;
		this.deliveryService = deliveryService;
	}

	@PostMapping("/orderStatus")
	public Order updateOrderStatus(@RequestParam int orderId, @RequestParam String status) throws Exception {
		if (orderId == 0)
			throw new IllegalArgumentException("Order Not Found");
		if (status == null)
			throw new IllegalArgumentException("Status not found for Updation");
		status=Constants.Status.OrderStatus.valueOf(status).name();
		Order order = restrauntService.updateOrderStatus(orderId, status);
		// mark the delivery boy as active again
		if (status.equals(Constants.Status.OrderStatus.DELIVERED.name()))
			deliveryService.updateDeliveryBoyStatus(order.getDeliveryBoyId(), Constants.Status.DeliveryBoyStatus.ACTIVE.name());
		return order;
	}

	@GetMapping("/activeDeliveryBoy")
	public List<DeliveryBoy> getDeliveryBoys() throws Exception {
		return deliveryService.getActiveDeliveryBoys();
	}

	@GetMapping("/deliveryBoy")
	public Map<String, Object> getDeliveryBoys(@RequestParam int deliveryBoyId) throws Exception {
		if (deliveryBoyId == 0)
			throw new IllegalArgumentException("Input Parameter not recieved");
		DeliveryBoy deliveryBoy = deliveryService.getDeliveryBoy(deliveryBoyId);
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("deliveryData", deliveryBoy);
		if (deliveryBoy.getStatus().equals(Constants.Status.DeliveryBoyStatus.INTRANSIT.name())) {
			Order order = restrauntService.getOrderInTransit(deliveryBoyId);
			map.put("orderData", order);
		}
		return map;
	}

	@PutMapping("/assignOrder")
	public Order assignOrderDelivery(@RequestParam int orderId, @RequestParam int deliveryBoyId) throws Exception {
		if (orderId == 0 || deliveryBoyId == 0)
			throw new IllegalArgumentException("Input not recieved");
		// mark the delivery boy as inactive
		deliveryService.updateDeliveryBoyStatus(deliveryBoyId, Constants.Status.DeliveryBoyStatus.INTRANSIT.name());
		return restrauntService.allotOrderToDeliveryBoy(orderId, deliveryBoyId);
	}
}
