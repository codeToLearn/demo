package com.example.repository;


import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import java.util.stream.Collectors;


import org.springframework.stereotype.Repository;

import com.example.model.Order;
import com.example.util.Constants;
@Repository
public class OrderDao {

	private static Map<Integer, Order> orderTable=new HashMap<Integer, Order>();
	
	public List<Order> getOrderByStatus(String status) {
		return orderTable.keySet().parallelStream().filter(a->orderTable.get(a).getStatus().equals(status)).map(a->orderTable.get(a)).collect(Collectors.toList());
	}
	
	public List<Order> getOrderByStatus(List<String> status) {
		Set<String> set=status.stream().collect(Collectors.toSet());
		return orderTable.keySet().parallelStream().filter(a->set.contains(orderTable.get(a).getStatus())).map(a->orderTable.get(a)).collect(Collectors.toList());
	}
	
	public int createOrder(Order order) {
		int id=Order.getCount().incrementAndGet();
		order.setId(id);
		orderTable.put(id, order);
		return id;
	}

	public Order updateOrderDetails(Order order) {
		// TODO Auto-generated method stub
		Order oldData=orderTable.get(order.getId());
		if(oldData==null)
			throw new IllegalArgumentException("Order Not Found ");
		if(order.getCustomerName()!=null && !"".equals(order.getCustomerName()))
			oldData.setCustomerName(order.getCustomerName());
		
		if(order.getProductName()!=null && !"".equals(order.getProductName()))
			oldData.setProductName(order.getProductName());
		
		if(order.getStatus()!=null && !"".equals(order.getStatus())) {
			oldData.setStatus(order.getStatus());
		}
		
		if(order.getPickedAt()!=null)
			oldData.setPickedAt(order.getPickedAt());
		
		if(order.getDeliveredAt()!=null)
			oldData.setDeliveredAt(order.getDeliveredAt());
		
		if(order.getQuantity()!=0)
			oldData.setQuantity(order.getQuantity());
		
		if(order.getDeliveryBoyId()!=0)
			oldData.setDeliveryBoyId(order.getDeliveryBoyId());
		
		orderTable.put(oldData.getId(), oldData);
		return oldData;
	}

	public Order getOrderDetails(Integer orderId) {
		// TODO Auto-generated method stub
		Order oldData=orderTable.get(orderId);
		if(oldData==null)
			throw new IllegalArgumentException("Order Not Found ");
		return oldData;
	}

	public List<Order> getOrderInTransit(int deliveryBoyId) {
		// TODO Auto-generated method stub
		return orderTable.keySet().parallelStream().
				filter(a->orderTable.get(a).getStatus().equals(Constants.Status.OrderStatus.PICKED.name()))
				.filter(a->orderTable.get(a).getDeliveryBoyId()==deliveryBoyId).map(a->orderTable.get(a))
				.collect(Collectors.toList());
	}
}
