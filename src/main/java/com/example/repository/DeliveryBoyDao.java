package com.example.repository;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import java.util.stream.Collectors;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Repository;

import com.example.model.DeliveryBoy;
import com.example.model.Order;
import com.example.util.Constants;

@Repository
public class DeliveryBoyDao {
	private static Map<Integer, DeliveryBoy> deliveryTable = new HashMap<Integer, DeliveryBoy>();

	private final Environment env;

	@Autowired
	public DeliveryBoyDao(Environment environment) {
		this.env = environment;
	}

	@PostConstruct
	private void createFirstDeliveryBoy() {
		int totalDel = env.getProperty(Constants.GlobalConst.NUMBER_OF_DELIVERY_BOYS) == null ? 0
				: Integer.parseInt(env.getProperty(Constants.GlobalConst.NUMBER_OF_DELIVERY_BOYS));
		if (totalDel != 0) {
			for (int i = 0; i < totalDel; i++) {
				System.out.println(
						createDeliveryBoy(new DeliveryBoy("D" + i, Constants.Status.DeliveryBoyStatus.ACTIVE.name()))
								+ "delivery boy added to database");
			}
		}
		// throws exception
	}

	public int createDeliveryBoy(DeliveryBoy deliveryBoy) {
		int id = Order.getCount().incrementAndGet();
		deliveryBoy.setId(id);
		deliveryTable.put(id, deliveryBoy);
		return id;
	}

	public DeliveryBoy updateDeliveryBoy(DeliveryBoy deliveryBoy) {
		deliveryTable.put(deliveryBoy.getId(), deliveryBoy);
		return deliveryBoy;
	}

	public List<DeliveryBoy> getDeliveryByStatus(String status) {
		return deliveryTable.keySet().parallelStream().filter(a -> status.equals(deliveryTable.get(a).getStatus()))
				.map(a -> deliveryTable.get(a)).collect(Collectors.toList());
	}

	public DeliveryBoy getDeliveryBoy(int deliveryBoyId) {

		return deliveryTable.get(deliveryBoyId);
	}
}
