package com.example.model;

import java.util.concurrent.atomic.AtomicInteger;

public class DeliveryBoy {
	private static final AtomicInteger count = new AtomicInteger(0);
	private int id;
	private String name;
	private String status;
	
	public DeliveryBoy( String name, String status) {
		super();
		this.name = name;
		this.status = status;
	}
	public static AtomicInteger getCount() {
		return count;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	
}
