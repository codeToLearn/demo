package com.example.model;

import java.io.Serializable;
import java.util.Date;
import java.util.concurrent.atomic.AtomicInteger;


public class Order implements Serializable{
	private static final AtomicInteger count = new AtomicInteger(0);
	private int id;
	private String productName;
	private String customerName;
	private int quantity;
	private Date placedAt;
	private Date pickedAt;
	private Date deliveredAt;
	private String status;
	private int deliveryBoyId;//mapped to DeliveryBoy.id
	
	
	public int getDeliveryBoyId() {
		return deliveryBoyId;
	}
	public void setDeliveryBoyId(int deliveryBoyId) {
		this.deliveryBoyId = deliveryBoyId;
	}
	public static AtomicInteger getCount() {
		return count;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public int getQuantity() {
		return quantity;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	public Date getPlacedAt() {
		return placedAt;
	}
	public void setPlacedAt(Date placedAt) {
		this.placedAt = placedAt;
	}
	public Date getPickedAt() {
		return pickedAt;
	}
	public void setPickedAt(Date pickedAt) {
		this.pickedAt = pickedAt;
	}
	public Date getDeliveredAt() {
		return deliveredAt;
	}
	public void setDeliveredAt(Date deliveredAt) {
		this.deliveredAt = deliveredAt;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	
	
}
